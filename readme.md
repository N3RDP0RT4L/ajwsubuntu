run this command to add this repo to `/etc/apt/sources.list.d` 

first aquire GPG key using.
```
wget -qO - https://gitlab.com/N3RDP0RT4L-HQ/ajwsubuntu/raw/master/ubuntu/9E82C33E.key | sudo apt-key add -
```

```
echo "deb [trusted=yes] https://gitlab.com/N3RDP0RT4L-HQ/ajwsubuntu/raw/master/ubuntu all main" | sudo tee /etc/apt/sources.list.d/ajwsubuntu.list
```


*I take no responisablity for the packages in this repo*
